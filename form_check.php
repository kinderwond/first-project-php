<?php
function Validate($data)
{
    $name   = "/[a-zA-Z]+/";
    $email  = "/[a-z]|[A-Z]*[a-zA-Z0-9]*@(mail|gmail|ukr).(com|net)$/";
    $result = array(
        'name' => '',
        'email' => ''
    );
    preg_match_all($name, $data['surname'], $matches, PREG_SET_ORDER, 0);
    foreach ($matches as $base_key => $base_value) {
        foreach ($base_value as $key => $value) {
            $result['name'] .= $value;
        }
    }
    preg_match_all($email, $data['email'], $matches, PREG_SET_ORDER, 0);
    for ($row = 0; $row < count($matches); $row++) 
    {
        $result['email'] .= $matches[$row][0];
    }
    if ($result['name'] == null || $result['email'] == null) 
    {
        return null;
    }
    return $result;
}
?>